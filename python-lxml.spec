%global _empty_manifest_terminate_build 0
%global _description \
The lxml XML toolkit is a Pythonic binding for the C libraries libxml2 and libxslt. \
It is unique in that it combines the speed and XML feature completeness of these libraries with \
the simplicity of a native Python API, mostly compatible but superior to the well-known ElementTree API. \
The latest release works with all CPython versions from 2.7 to 3.7.

Name:		python-lxml
Version:	5.3.0
Release:	1
Summary:	XML processing library combining libxml2/libxslt with the ElementTree API
License:	BSD
URL:		https://github.com/lxml/lxml
Source0:	https://files.pythonhosted.org/packages/e7/6b/20c3a4b24751377aaa6307eb230b66701024012c29dd374999cc92983269/lxml-5.3.0.tar.gz

%description
%{_description}

%package -n python3-lxml
Summary:	XML processing library combining libxml2/libxslt with the ElementTree API
Provides:	python-lxml = %{version}-%{release}
BuildRequires:	gcc
BuildRequires:	libxml2-devel
BuildRequires:	libxslt-devel
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-Cython
%description -n python3-lxml
%{_description}

%package help
Summary:	Development documents and examples for lxml
Provides:	python3-lxml-doc
%description help
%{_description}

%prep
%autosetup -n lxml-%{version} -p1
find -type f -name '*.c' -print -delete

%build
%py3_build

%install
%py3_install
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
make test

%files -n python3-lxml -f filelist.lst
%license doc/licenses/*.txt LICENSES.txt
%dir %{python3_sitearch}/*

%files help -f doclist.lst
%doc README.rst src/lxml/isoschematron/resources/xsl/iso-schematron-xslt1/readme.txt

%changelog
* Mon Sep 09 2024 lixiaoyong <lixiaoyong@kylinos.cn> - 5.3.0-1
- upgrade version to 5.3.0
  - GH#421: Nested CDATA sections are no longer rejected but split on output to represent ]]> correctly. Patch by Gertjan Klein
  - LP#2060160: Attribute values serialised differently in xmlfile.element() and xmlfile.write()
  - LP#2058177: The ISO-Schematron implementation could fail on unknown prefixes. Patch by David Lakin


* Wed Aug 28 2024 zhuofeng <zhuofeng2@huawei.com> - 5.2.2-2
- fix failed test

* Thu May 23 2024 wangxiaomeng <wangxiaomeng@kylinos.cn> - 5.2.2-1
- upgrade version to 5.2.2
  - GH#417: The test_feed_parser test could fail if lxml_html_clean was not installed. It is now skipped in that case.
  - LP#2059910: The minimum CPU architecture for the Linux x86 binary wheels was set back to "core2", without SSE 4.2.
  - If libxml2 uses iconv, the compile time version is available as etree.ICONV_COMPILED_VERSION.

* Fri May 10 2024 wangxiaomeng <wangxiaomeng@kylinos.cn> - 5.2.1-1
- upgrade version to 5.2.1
  - LP#2059910: The minimum CPU architecture for the Linux x86 binary wheels was set back to "core2", but with SSE 4.2 enabled
  - LP#2059977: "Element.iterfind("//absolute_path")" failed with a "SyntaxError" where it should have issued a warning
  - GH#416: The documentation build was using the non-standard ``which`` command. Patch by Michał Górny.
  - The minimum CPU architecture for the Linux x86 binary wheels was upgraded to "sandybridge" (launched 2011), and glibc 2.28 / gcc 12 (manylinux_2_28) wheels were added.
  - Built with Cython 3.0.10

* Tue Apr 23 2024 zhangzikang <zhangzikang@kylinos.cn> - 5.1.0-2
- Fix test_elementtree with Expat-2.6.0, resolve check error

* Wed Feb 07 2024 dongyuzhen <dongyuzhen@h-partners.com> - 5.1.0-1
- upgrade version to 5.1.0:
  - some incorrect declarations were removed from ``python.pxd``
  - built with Cython 3.0.7
  - some redundant and long deprecated methods were removed
  - character escaping in ``C14N2`` serialisation now uses a single pass over the text instead of searching for each unescaped character separately
  - early support for Python 3.13a2 was added
  - support for Python 2.7 and Python versions < 3.6 was removed
  - parsing ASCII strings is slightly faster
  - some bugs fixes

* Wed Aug 09 2023 zhuofeng <zhuofeng2@huawei.com> - 4.9.3-2
- sync fedara patch

* Wed Jul 12 2023 sunhui <sunhui@kylinos.cn> - 4.9.3-1
- Update package to version 4.9.3

* Wed Dec 14 2022 wangjunqi <wangjunqi@kylinos.cn> - 4.9.2-1
- Update package to version 4.9.2

* Fri Jul 29 2022 renhongxun <renhongxun@h-partners.com> - 4.9.1-3
- add amended patch for cve-2022-2309

* Mon Jul 25 2022 shixuantong <shixuantong@h-partners.com> - 4.9.1-2
- Remove pregenerated Cython C sources

* Mon Jul 25 2022 liksh <liks11@chinaunicom.cn> - 4.9.1-1
- Upgrade to 4.9.1 for openstack yoga

* Wed Jan 19 2022 shixuantong <shixuantong@huawei.com> - 4.7.1-2
- enable check

* Sat Dec 25 2021 liudabo<liudabo1@huawei.com> - 4.7.1-1
- DESC: upgrade python-lxml to 4.7.1

* Mon Dec 13 2021 hanxinke<hanxinke@huawei.com> - 4.6.5-1
- DESC: upgrade python-lxml to 4.6.5

* Wed Apr 14 2021 shixuantong<shixuantong@huawei.com> - 4.6.2-2
- fix CVE-2021-28957

* Mon Feb 1 2021 wangjie<wangjie294@huawei.com> - 4.6.2-1
- upgrade  4.6.2-1

* Fri Oct 30 2020 wuchaochao <wuchaochao4@huawei.com> - 4.5.2-2
- Type:bufix
- CVE:NA
- SUG:NA
- DESC:remove python2

* Thu Jul 23 2020 tianwei  <tianwei12@huawei.com> - 4.5.2-1
- Package update to release 4.5.2
* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.2.3-3
- Package init
